//
//  APINormalResponseTest.swift
//  prova-improving-tests
//
//  Created by Thalles Araújo on 07/02/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.
//

import Quick
import Nimble

class APINormalResponseTest: QuickSpec{
    
    override func spec() {
     
        describe("get issues") {
            context("opening app", {
                it("should load issues", closure: {
                    
                    let asyncExpectation = self.expectation(description: "Async block executed")
                    DispatchQueue.main.async {
                        IssueService.getIssues(sucesso: { (issues) in
                            expect(issues.count).to(beGreaterThan(0))
                            asyncExpectation.fulfill()
                        }, falha: { (error) in
                            expect(error).toNot(equal("Falha ao ler JSON"))
                        })
                    }
                    self.waitForExpectations(timeout: 5, handler: nil)
                    
                })
            })
        }
    }
    
}
