//
//  IssueTest.swift
//  prova-improving
//
//  Created by Thalles Araújo on 26/01/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.
//

import XCTest
import Nimble
import Cuckoo

class IssueTest: XCTestCase {
    
    func testIfPresent(){
        IssueService.getIssues(sucesso: { (issues) in
            let issue: Issue = Issue()
            issue.id = 0
            expect(issues.filter({issue in return issue.id == 555187621}).first!.id).notTo(equal(issue.id))
        }) { (error) in
            XCTFail()
        }
        
    }

    func testIfUserNameMatches(){
        IssueService.getIssues(sucesso: { (issues) in
            expect(issues.filter({issue in return issue.id == 555187621}).first!.usuarioCriador!.username).to(equal("buttaface"))
        }) { (error) in
            XCTFail()
        }
    }
    
    func testIfNotPresent(){
        let issue = MockIssue()
        IssueService.getIssues(sucesso: { (issues) in
            expect(issues.filter({issue in return issue.id == 555187621}).first!.usuarioCriador!.username).toNot(equal(issue.usuarioCriador?.username))
        }) { (error) in
            XCTFail()
        }
        
        
    }


}
