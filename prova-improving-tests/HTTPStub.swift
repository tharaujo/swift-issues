//
//  HTTPStub.swift
//  prova-improving-tests
//
//  Created by Thalles Araújo on 26/01/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.
//

import Foundation
import OHHTTPStubs

class HTTPStub{
    
    static func errorGetStub(){
        stub(condition: isMethodGET()) { _ in
            
            return HTTPStubsResponse(
                fileAtPath: "",
                statusCode: 500,
                headers: [ "Content-Type": "application/json" ]
            )
        }
    }
    
    static func errorInvalidJsonStub(){
        stub(condition: isMethodGET()) { _ in
            
            let wrongFormattedJson = [
                "this json": "is invalid"
            ]
            
            return HTTPStubsResponse(
                jsonObject: wrongFormattedJson,
                statusCode: 200,
                headers: [ "Content-Type": "application/json" ]
            )
        }
    }
    
}
