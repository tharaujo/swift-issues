//
//  APITest.swift
//  prova-improving-tests
//
//  Created by Thalles Araújo on 26/01/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.
//

import XCTest
import OHHTTPStubs
import Nimble

class APITest: XCTestCase {
    func testErrorResponse(){
        
        func testRaceCondition() {
            let asyncExpectation = expectation(description: "Async block executed")
            DispatchQueue.main.async {
                HTTPStub.errorGetStub()
                IssueService.getIssues(sucesso: { (issues) in
                    XCTFail()
                }) { (error) in
                    expect(error).to(equal("Falha ao buscar dados"))
                    asyncExpectation.fulfill()
                }
            }
            waitForExpectations(timeout: 5, handler: {_ in })
        }
    
    }
    
    
    func testInvalidJson(){
        
        
        func testRaceCondition() {
            let asyncExpectation = expectation(description: "Async block executed")
            DispatchQueue.main.async {
                HTTPStub.errorInvalidJsonStub()
                IssueService.getIssues(sucesso: { (issues) in
                    XCTFail()
                }) { (error) in
                    expect(error).to(equal("Erro ao ler JSON"))
                    asyncExpectation.fulfill()
                }
            }
            waitForExpectations(timeout: 5, handler: nil)
        }
        
        
    }
    

}
