// MARK: - Mocks generated from file: prova-improving/Models/Issue.swift at 2020-02-07 20:27:15 +0000

//
//  Issue.swift
//  prova-improving
//
//  Created by Thalles Araújo on 26/01/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.

import Cuckoo
@testable import prova_improving

import Foundation
import ObjectMapper


 class MockIssue: Issue, Cuckoo.ClassMock {
    
     typealias MocksType = Issue
    
     typealias Stubbing = __StubbingProxy_Issue
     typealias Verification = __VerificationProxy_Issue

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: true)

    
    private var __defaultImplStub: Issue?

     func enableDefaultImplementation(_ stub: Issue) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    
    
    
     override var id: Int {
        get {
            return cuckoo_manager.getter("id",
                superclassCall:
                    
                    super.id
                    ,
                defaultCall: __defaultImplStub!.id)
        }
        
        set {
            cuckoo_manager.setter("id",
                value: newValue,
                superclassCall:
                    
                    super.id = newValue
                    ,
                defaultCall: __defaultImplStub!.id = newValue)
        }
        
    }
    
    
    
     override var titulo: String {
        get {
            return cuckoo_manager.getter("titulo",
                superclassCall:
                    
                    super.titulo
                    ,
                defaultCall: __defaultImplStub!.titulo)
        }
        
        set {
            cuckoo_manager.setter("titulo",
                value: newValue,
                superclassCall:
                    
                    super.titulo = newValue
                    ,
                defaultCall: __defaultImplStub!.titulo = newValue)
        }
        
    }
    
    
    
     override var estado: String {
        get {
            return cuckoo_manager.getter("estado",
                superclassCall:
                    
                    super.estado
                    ,
                defaultCall: __defaultImplStub!.estado)
        }
        
        set {
            cuckoo_manager.setter("estado",
                value: newValue,
                superclassCall:
                    
                    super.estado = newValue
                    ,
                defaultCall: __defaultImplStub!.estado = newValue)
        }
        
    }
    
    
    
     override var descricao: String {
        get {
            return cuckoo_manager.getter("descricao",
                superclassCall:
                    
                    super.descricao
                    ,
                defaultCall: __defaultImplStub!.descricao)
        }
        
        set {
            cuckoo_manager.setter("descricao",
                value: newValue,
                superclassCall:
                    
                    super.descricao = newValue
                    ,
                defaultCall: __defaultImplStub!.descricao = newValue)
        }
        
    }
    
    
    
     override var usuarioCriador: UsuarioCriador? {
        get {
            return cuckoo_manager.getter("usuarioCriador",
                superclassCall:
                    
                    super.usuarioCriador
                    ,
                defaultCall: __defaultImplStub!.usuarioCriador)
        }
        
        set {
            cuckoo_manager.setter("usuarioCriador",
                value: newValue,
                superclassCall:
                    
                    super.usuarioCriador = newValue
                    ,
                defaultCall: __defaultImplStub!.usuarioCriador = newValue)
        }
        
    }
    
    
    
     override var dataCriacao: String {
        get {
            return cuckoo_manager.getter("dataCriacao",
                superclassCall:
                    
                    super.dataCriacao
                    ,
                defaultCall: __defaultImplStub!.dataCriacao)
        }
        
        set {
            cuckoo_manager.setter("dataCriacao",
                value: newValue,
                superclassCall:
                    
                    super.dataCriacao = newValue
                    ,
                defaultCall: __defaultImplStub!.dataCriacao = newValue)
        }
        
    }
    
    
    
     override var link: String {
        get {
            return cuckoo_manager.getter("link",
                superclassCall:
                    
                    super.link
                    ,
                defaultCall: __defaultImplStub!.link)
        }
        
        set {
            cuckoo_manager.setter("link",
                value: newValue,
                superclassCall:
                    
                    super.link = newValue
                    ,
                defaultCall: __defaultImplStub!.link = newValue)
        }
        
    }
    

    

    
    
    
     override func mapping(map: Map)  {
        
    return cuckoo_manager.call("mapping(map: Map)",
            parameters: (map),
            escapingParameters: (map),
            superclassCall:
                
                super.mapping(map: map)
                ,
            defaultCall: __defaultImplStub!.mapping(map: map))
        
    }
    

	 struct __StubbingProxy_Issue: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    var id: Cuckoo.ClassToBeStubbedProperty<MockIssue, Int> {
	        return .init(manager: cuckoo_manager, name: "id")
	    }
	    
	    
	    var titulo: Cuckoo.ClassToBeStubbedProperty<MockIssue, String> {
	        return .init(manager: cuckoo_manager, name: "titulo")
	    }
	    
	    
	    var estado: Cuckoo.ClassToBeStubbedProperty<MockIssue, String> {
	        return .init(manager: cuckoo_manager, name: "estado")
	    }
	    
	    
	    var descricao: Cuckoo.ClassToBeStubbedProperty<MockIssue, String> {
	        return .init(manager: cuckoo_manager, name: "descricao")
	    }
	    
	    
	    var usuarioCriador: Cuckoo.ClassToBeStubbedOptionalProperty<MockIssue, UsuarioCriador> {
	        return .init(manager: cuckoo_manager, name: "usuarioCriador")
	    }
	    
	    
	    var dataCriacao: Cuckoo.ClassToBeStubbedProperty<MockIssue, String> {
	        return .init(manager: cuckoo_manager, name: "dataCriacao")
	    }
	    
	    
	    var link: Cuckoo.ClassToBeStubbedProperty<MockIssue, String> {
	        return .init(manager: cuckoo_manager, name: "link")
	    }
	    
	    
	    func mapping<M1: Cuckoo.Matchable>(map: M1) -> Cuckoo.ClassStubNoReturnFunction<(Map)> where M1.MatchedType == Map {
	        let matchers: [Cuckoo.ParameterMatcher<(Map)>] = [wrap(matchable: map) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockIssue.self, method: "mapping(map: Map)", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_Issue: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	    
	    var id: Cuckoo.VerifyProperty<Int> {
	        return .init(manager: cuckoo_manager, name: "id", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    
	    var titulo: Cuckoo.VerifyProperty<String> {
	        return .init(manager: cuckoo_manager, name: "titulo", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    
	    var estado: Cuckoo.VerifyProperty<String> {
	        return .init(manager: cuckoo_manager, name: "estado", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    
	    var descricao: Cuckoo.VerifyProperty<String> {
	        return .init(manager: cuckoo_manager, name: "descricao", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    
	    var usuarioCriador: Cuckoo.VerifyOptionalProperty<UsuarioCriador> {
	        return .init(manager: cuckoo_manager, name: "usuarioCriador", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    
	    var dataCriacao: Cuckoo.VerifyProperty<String> {
	        return .init(manager: cuckoo_manager, name: "dataCriacao", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    
	    var link: Cuckoo.VerifyProperty<String> {
	        return .init(manager: cuckoo_manager, name: "link", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	
	    
	    @discardableResult
	    func mapping<M1: Cuckoo.Matchable>(map: M1) -> Cuckoo.__DoNotUse<(Map), Void> where M1.MatchedType == Map {
	        let matchers: [Cuckoo.ParameterMatcher<(Map)>] = [wrap(matchable: map) { $0 }]
	        return cuckoo_manager.verify("mapping(map: Map)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class IssueStub: Issue {
    
    
     override var id: Int {
        get {
            return DefaultValueRegistry.defaultValue(for: (Int).self)
        }
        
        set { }
        
    }
    
    
     override var titulo: String {
        get {
            return DefaultValueRegistry.defaultValue(for: (String).self)
        }
        
        set { }
        
    }
    
    
     override var estado: String {
        get {
            return DefaultValueRegistry.defaultValue(for: (String).self)
        }
        
        set { }
        
    }
    
    
     override var descricao: String {
        get {
            return DefaultValueRegistry.defaultValue(for: (String).self)
        }
        
        set { }
        
    }
    
    
     override var usuarioCriador: UsuarioCriador? {
        get {
            return DefaultValueRegistry.defaultValue(for: (UsuarioCriador?).self)
        }
        
        set { }
        
    }
    
    
     override var dataCriacao: String {
        get {
            return DefaultValueRegistry.defaultValue(for: (String).self)
        }
        
        set { }
        
    }
    
    
     override var link: String {
        get {
            return DefaultValueRegistry.defaultValue(for: (String).self)
        }
        
        set { }
        
    }
    

    

    
     override func mapping(map: Map)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}


// MARK: - Mocks generated from file: prova-improving/Models/UsuarioCriador.swift at 2020-02-07 20:27:15 +0000

//
//  UsuarioCriador.swift
//  prova-improving
//
//  Created by Thalles Araújo on 26/01/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.

import Cuckoo
@testable import prova_improving

import Foundation
import ObjectMapper


 class MockUsuarioCriador: UsuarioCriador, Cuckoo.ClassMock {
    
     typealias MocksType = UsuarioCriador
    
     typealias Stubbing = __StubbingProxy_UsuarioCriador
     typealias Verification = __VerificationProxy_UsuarioCriador

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: true)

    
    private var __defaultImplStub: UsuarioCriador?

     func enableDefaultImplementation(_ stub: UsuarioCriador) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    
    
    
     override var avatar: String {
        get {
            return cuckoo_manager.getter("avatar",
                superclassCall:
                    
                    super.avatar
                    ,
                defaultCall: __defaultImplStub!.avatar)
        }
        
        set {
            cuckoo_manager.setter("avatar",
                value: newValue,
                superclassCall:
                    
                    super.avatar = newValue
                    ,
                defaultCall: __defaultImplStub!.avatar = newValue)
        }
        
    }
    
    
    
     override var username: String {
        get {
            return cuckoo_manager.getter("username",
                superclassCall:
                    
                    super.username
                    ,
                defaultCall: __defaultImplStub!.username)
        }
        
        set {
            cuckoo_manager.setter("username",
                value: newValue,
                superclassCall:
                    
                    super.username = newValue
                    ,
                defaultCall: __defaultImplStub!.username = newValue)
        }
        
    }
    

    

    
    
    
     override func mapping(map: Map)  {
        
    return cuckoo_manager.call("mapping(map: Map)",
            parameters: (map),
            escapingParameters: (map),
            superclassCall:
                
                super.mapping(map: map)
                ,
            defaultCall: __defaultImplStub!.mapping(map: map))
        
    }
    

	 struct __StubbingProxy_UsuarioCriador: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    var avatar: Cuckoo.ClassToBeStubbedProperty<MockUsuarioCriador, String> {
	        return .init(manager: cuckoo_manager, name: "avatar")
	    }
	    
	    
	    var username: Cuckoo.ClassToBeStubbedProperty<MockUsuarioCriador, String> {
	        return .init(manager: cuckoo_manager, name: "username")
	    }
	    
	    
	    func mapping<M1: Cuckoo.Matchable>(map: M1) -> Cuckoo.ClassStubNoReturnFunction<(Map)> where M1.MatchedType == Map {
	        let matchers: [Cuckoo.ParameterMatcher<(Map)>] = [wrap(matchable: map) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockUsuarioCriador.self, method: "mapping(map: Map)", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_UsuarioCriador: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	    
	    var avatar: Cuckoo.VerifyProperty<String> {
	        return .init(manager: cuckoo_manager, name: "avatar", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    
	    var username: Cuckoo.VerifyProperty<String> {
	        return .init(manager: cuckoo_manager, name: "username", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	
	    
	    @discardableResult
	    func mapping<M1: Cuckoo.Matchable>(map: M1) -> Cuckoo.__DoNotUse<(Map), Void> where M1.MatchedType == Map {
	        let matchers: [Cuckoo.ParameterMatcher<(Map)>] = [wrap(matchable: map) { $0 }]
	        return cuckoo_manager.verify("mapping(map: Map)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class UsuarioCriadorStub: UsuarioCriador {
    
    
     override var avatar: String {
        get {
            return DefaultValueRegistry.defaultValue(for: (String).self)
        }
        
        set { }
        
    }
    
    
     override var username: String {
        get {
            return DefaultValueRegistry.defaultValue(for: (String).self)
        }
        
        set { }
        
    }
    

    

    
     override func mapping(map: Map)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}

