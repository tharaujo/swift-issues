//
//  VCIssue.swift
//  prova-improving
//
//  Created by Thalles Araújo on 26/01/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.
//

import UIKit
import Kingfisher

class VCIssue: UIViewController, Storyboarded {
    
    var issue: Issue?
    weak var coordinator: MainCoordinator?

    @IBOutlet weak var avatarUsuarioCriador: UIImageView!
    @IBOutlet weak var nomeUsuarioCriador: UILabel!
    
    @IBOutlet weak var tituloIssue: UILabel!
    @IBOutlet weak var dataCriacaoIssue: UILabel!
    @IBOutlet weak var descricaoIssue: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.config(issue: self.issue)
    }
    
    private func config(issue: Issue?){
        
        self.avatarUsuarioCriador.kf.setImage(with: URL.init(string: issue?.usuarioCriador?.avatar ?? "avatar-placeholder"), placeholder: UIImage.init(named: "avatar-placeholder"), options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
        self.nomeUsuarioCriador.text = issue?.usuarioCriador?.username
        
        self.tituloIssue.text = issue?.titulo
        if let dataCriacao = issue?.dataCriacao{
            self.dataCriacaoIssue.text = "Criada em: \(dataCriacao.formatBr())"
        }
        self.descricaoIssue.text = issue?.descricao
        self.title = "#\(issue!.id)"
    }
    
    @IBAction func abrirIssueNoBrowser(_ sender: UIButton) {
        if let url = self.issue?.link {
            UIApplication.shared.open(URL(string: url)!, options: [:]) { _ in }
        }
    }
    
   
}
