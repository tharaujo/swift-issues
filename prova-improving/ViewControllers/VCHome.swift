//
//  VCHome.swift
//  prova-improving
//
//  Created by Thalles Araújo on 26/01/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.
//

import UIKit

class VCHome: UIViewController, UITableViewDelegate, UITableViewDataSource, Storyboarded {
    
    @IBOutlet weak var issuesTableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    weak var coordinator: MainCoordinator?
    
    var issues: [Issue] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.issuesTableView.accessibilityIdentifier = "IssuesTableView"
        self.issuesTableView.isAccessibilityElement = true
        self.issuesTableView.delegate = self
        self.issuesTableView.dataSource = self
        self.spinner.startAnimating()
        self.loadIssues {
            self.issuesTableView.reloadData()
            self.spinner.stopAnimating()
            self.spinner.isHidden = true
        }
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return self.issues.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "issueCell") as? IssueCell{
            cell.config(issue: self.issues[indexPath.row])
            return cell
        }else{
            return IssueCell()
        }
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let issue = self.issues[indexPath.row]
        self.coordinator?.goToIssue(issue: issue)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    
    func loadIssues(completion: @escaping ()-> Void){
        let dispatcher = DispatchGroup()
        
        dispatcher.enter()
        IssueService.getIssues(sucesso: { (issues) in
            self.issues = issues
            dispatcher.leave()
        }) { (falha) in
            let alert = UIAlertController.init(title: "Ocorreu um problema", message: falha, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (action) in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true)
            dispatcher.leave()
        }
        
        dispatcher.notify(queue: .main) {
            completion()
        }
    }
    

}
