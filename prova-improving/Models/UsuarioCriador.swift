//
//  UsuarioCriador.swift
//  prova-improving
//
//  Created by Thalles Araújo on 26/01/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.
//

import Foundation
import ObjectMapper
class UsuarioCriador: Mappable{
    
    var avatar: String = ""
    var username: String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        avatar <- map["avatar_url"]
        username <- map["login"]
    }
    
}
