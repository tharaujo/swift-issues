//
//  Issue.swift
//  prova-improving
//
//  Created by Thalles Araújo on 26/01/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.
//

import Foundation
import ObjectMapper
infix operator <-

class Issue: Mappable{
    
    var id: Int = 0
    var titulo: String = ""
    var estado: String = ""
    var descricao: String = ""
    var usuarioCriador: UsuarioCriador?
    var dataCriacao: String = ""
    var link: String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        titulo <- map["title"]
        estado <- map["state"]
        descricao <- map["body"]
        usuarioCriador <- map["user"]
        dataCriacao <- map["created_at"]
        link <- map["html_url"]
    }
    
}
