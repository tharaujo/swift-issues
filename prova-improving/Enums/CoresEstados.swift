//
//  CoresEstados.swift
//  prova-improving
//
//  Created by Thalles Araújo on 07/02/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.
//

import UIKit
enum CoresEstados{
    
    case open
    case closed
    
    
    init(estado: String){
        switch estado {
        case "open":
            self = .open
        default:
            self = .closed
        }
    }
}

extension CoresEstados{
    var colorValue: UIColor {
        get {
            switch self {
            case .open:
                return UIColor.orange
            case .closed:
                return UIColor.green
            }
        }
    }
}
