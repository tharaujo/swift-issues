//
//  Estados.swift
//  prova-improving
//
//  Created by Thalles Araújo on 07/02/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.
//

import Foundation

enum Estados: String{
    
    case open = "ABERTO"
    case closed = "FECHADO"
    
    init(estado: String){
        switch estado {
        case "open":
            self = .open
        default:
            self = .closed
        }
    }
    
}
