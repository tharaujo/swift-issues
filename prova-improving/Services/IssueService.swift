//
//  IssueService.swift
//  prova-improving
//
//  Created by Thalles Araújo on 26/01/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class IssueService{
    
    class func getIssues(sucesso: @escaping (_ issues: [Issue]) -> Void, falha: @escaping (_ erro: String) -> Void){
        
        
        Network.request(url: APIHelper.ISSUES.getURL(), method: .get, completion: { (response) in
            guard let statusCode = response.response?.statusCode, statusCode == 200 else {
                falha("Falha ao buscar dados")
                return
            }
            
            guard let result = response.result.value! as? [[String:Any]] else {
                falha("Erro ao ler JSON")
                return
            }
            
            let issues = Mapper<Issue>().mapArray(JSONArray: result)
            sucesso(issues)
        }, noNetworkCompletion: falha)
    }
    
    
}
