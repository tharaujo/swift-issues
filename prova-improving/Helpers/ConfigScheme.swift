//
//  ConfigScheme.swift
//  prova-improving
//
//  Created by Thalles Araújo on 26/01/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.
//

import Foundation
class ConfigScheme{
    static var GITHUB_URL: String  {
        get {
            return ConfigScheme.valueForKey("GITHUB_URL")
        }
    }
    
    static func valueForKey(_ key: String, plistName: String = "Info") -> String {
        if let path = Bundle.main.path(forResource: plistName, ofType: "plist") {
            if let dic = NSDictionary(contentsOfFile: path) {
                return dic[key] as? String ?? ""
            }
        }
        
        return ""
    }
    
}
