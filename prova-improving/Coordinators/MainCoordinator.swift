//
//  MainCoordinator.swift
//  prova-improving
//
//  Created by Thalles Araújo on 08/02/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.
//

import UIKit

class MainCoordinator: Coordinator {
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController){
        self.navigationController = navigationController
        configureNavigationControllerApppearance()
    }
    
    func start() {
        let vc = VCHome.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    
    func goToIssue(issue: Issue){
        let vc = VCIssue.instantiate()
        vc.issue = issue
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }

}

extension MainCoordinator{
    
    func configureNavigationControllerApppearance(){
        self.navigationController.navigationBar.barStyle = .blackTranslucent
    }
    
}
