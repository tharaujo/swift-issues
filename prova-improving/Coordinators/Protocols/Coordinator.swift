//
//  Coordinator.swift
//  prova-improving
//
//  Created by Thalles Araújo on 08/02/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.
//

import UIKit

//Mais informações
//https://www.hackingwithswift.com/articles/71/how-to-use-the-coordinator-pattern-in-ios-apps

protocol Coordinator {
    
    var childCoordinators: [Coordinator] {get set}
    
    var navigationController: UINavigationController {get set}
    
    func start()

}
