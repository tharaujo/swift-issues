//
//  CustomTableViewDelegate.swift
//  prova-improving
//
//  Created by Thalles Araújo on 26/01/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.
//

import UIKit

protocol CustomTableViewDelegate: UITableViewDelegate{
    
    
    func didTapIssueCell(issueCell: IssueCell)
    
}
