//
//  IssueCell.swift
//  prova-improving
//
//  Created by Thalles Araújo on 26/01/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.
//

import UIKit
import Kingfisher

class IssueCell: UITableViewCell {
    
    
    @IBOutlet weak var tituloIssue: UILabel!
    @IBOutlet weak var descricaoIssue: UILabel!
    @IBOutlet weak var statusIssue: UILabel!
    @IBOutlet weak var avatarUsuario: UIImageView!

    func config(issue: Issue?){
        self.tituloIssue.text = issue?.titulo
        self.descricaoIssue.text = issue?.descricao
        
        if let estado = issue?.estado{
            self.statusIssue.text = "Estado: \(Estados.init(estado: estado).rawValue)"
        }
        
        
        self.statusIssue.textColor = CoresEstados(estado:issue!.estado).colorValue
        self.avatarUsuario.kf.setImage(with: URL.init(string: issue?.usuarioCriador?.avatar ?? "avatar-placeholder"), placeholder: UIImage.init(named: "avatar-placeholder"), options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    
    }

}
