//
//  EnumExtension.swift
//  prova-improving
//
//  Created by Thalles Araújo on 07/02/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.
//

import Foundation
extension StringEnum: StringLiteralConvertible {
    
    init(stringLiteral value: String){
        self.init(rawValue: value)!
    }
    
    init(extendedGraphemeClusterLiteral value: String) {
        self.init(stringLiteral: value)
    }
    
    init(unicodeScalarLiteral value: String) {
        self.init(stringLiteral: value)
    }
}
