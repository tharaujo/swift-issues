//
//  MainUITest.swift
//  prova-improving-uitests
//
//  Created by Thalles Araújo on 07/02/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.
//

import XCTest

class MainUITest: KIFTestCase {

    func testGoToIssue(){
        
        sleep(10)
        
        tester().tapRow(at: IndexPath.init(row: 0, section: 0), inTableViewWithAccessibilityIdentifier: "IssuesTableView")
    }
   
}
