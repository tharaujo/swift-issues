//
//  KIFSwiftExtension.swift
//  prova-improving-uitests
//
//  Created by Thalles Araújo on 07/02/20.
//  Copyright © 2020 Thalles Araújo. All rights reserved.
//

import XCTest

extension XCTestCase {
    func tester(file : String = #file, _ line : Int = #line) -> KIFUITestActor {
        return KIFUITestActor(inFile: file, atLine: line, delegate: self)
    }
}

extension KIFTestActor {
    func tester(file : String = #file, _ line : Int = #line) -> KIFUITestActor {
        return KIFUITestActor(inFile: file, atLine: line, delegate: self)
    }
}
